package com.example.demo.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IAM_ACCOUNT")
public class IamAccount {

	@Id
	private String refno;
	private String countryCode;
	private String accountCreationType;
	private String firstname;
	private String lastname;
	private Date lastLogin;
	private Date suspensionDate;
	private String suspensionFlag;
	private String id;
	private String accountRole;
	private String allowBuyNonSgListedEtf;
	private String email;
	private String status;
	private String dormant;
	private String verifiedEmailFlag;
	private String accountPlatform;
	private String accountType;
	private String isDpmsFlag;
	private Date approveDate;
	private String nric;
	private Date joinDate;
	
	public IamAccount(){}
	
	public IamAccount(String refno, String countryCode, String accountCreationType, String firstname, String lastname,
			Date lastLogin, Date suspensionDate, String suspensionFlag, String id, String accountRole,
			String allowBuyNonSgListedEtf, String email, String status, String dormant, String verifiedEmailFlag,
			String accountPlatform, String accountType, String isDpmsFlag, Date approveDate, String nric,
			Date joinDate) {
		super();
		this.refno = refno;
		this.countryCode = countryCode;
		this.accountCreationType = accountCreationType;
		this.firstname = firstname;
		this.lastname = lastname;
		this.lastLogin = lastLogin;
		this.suspensionDate = suspensionDate;
		this.suspensionFlag = suspensionFlag;
		this.id = id;
		this.accountRole = accountRole;
		this.allowBuyNonSgListedEtf = allowBuyNonSgListedEtf;
		this.email = email;
		this.status = status;
		this.dormant = dormant;
		this.verifiedEmailFlag = verifiedEmailFlag;
		this.accountPlatform = accountPlatform;
		this.accountType = accountType;
		this.isDpmsFlag = isDpmsFlag;
		this.approveDate = approveDate;
		this.nric = nric;
		this.joinDate = joinDate;
	}

	public String getRefno() {
		return refno;
	}

	public void setRefno(String refno) {
		this.refno = refno;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getAccountCreationType() {
		return accountCreationType;
	}

	public void setAccountCreationType(String accountCreationType) {
		this.accountCreationType = accountCreationType;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Date getSuspensionDate() {
		return suspensionDate;
	}

	public void setSuspensionDate(Date suspensionDate) {
		this.suspensionDate = suspensionDate;
	}

	public String getSuspensionFlag() {
		return suspensionFlag;
	}

	public void setSuspensionFlag(String suspensionFlag) {
		this.suspensionFlag = suspensionFlag;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountRole() {
		return accountRole;
	}

	public void setAccountRole(String accountRole) {
		this.accountRole = accountRole;
	}

	public String getAllowBuyNonSgListedEtf() {
		return allowBuyNonSgListedEtf;
	}

	public void setAllowBuyNonSgListedEtf(String allowBuyNonSgListedEtf) {
		this.allowBuyNonSgListedEtf = allowBuyNonSgListedEtf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDormant() {
		return dormant;
	}

	public void setDormant(String dormant) {
		this.dormant = dormant;
	}

	public String getVerifiedEmailFlag() {
		return verifiedEmailFlag;
	}

	public void setVerifiedEmailFlag(String verifiedEmailFlag) {
		this.verifiedEmailFlag = verifiedEmailFlag;
	}

	public String getAccountPlatform() {
		return accountPlatform;
	}

	public void setAccountPlatform(String accountPlatform) {
		this.accountPlatform = accountPlatform;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getIsDpmsFlag() {
		return isDpmsFlag;
	}

	public void setIsDpmsFlag(String isDpmsFlag) {
		this.isDpmsFlag = isDpmsFlag;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public String getNric() {
		return nric;
	}

	public void setNric(String nric) {
		this.nric = nric;
	}

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
}
