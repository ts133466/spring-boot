package com.example.demo.restc;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.entity.IamAccount;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@RestController
@RequestMapping("/user")
public class UserRestController {
	
	@Autowired
	RestTemplate restTemplate;
	
//	@PostMapping("/add")
//	public ResultModel add(@RequestParam String firstname, @RequestParam String lastname) {
//		String username = firstname + " " + lastname;
//		User user = restTemplate.getForObject("http://repository-service/user/get?username={username}", 
//				User.class, username);
//		ResultModel resultModel = new ResultModel();
//		
//		if(user != null){
//			resultModel.setStatus(ResultModel.Status.FAIL.toString());
//			resultModel.setMessage("user existed");
//		} else {
//			user = new User(firstname, lastname);
//			ResponseEntity<User> res = restTemplate.exchange("http://repository-service/user/add?firstname={firstname}&lastname={lastname}", 
//					HttpMethod.POST, null, User.class, firstname, lastname);
//			resultModel.setStatus(ResultModel.Status.SUCCESS.toString());
//			resultModel.setData(res.getBody());
//		}
//		return resultModel;
//	}
//	
//	@GetMapping("/find")
//	public List<User> find() {
//		ResponseEntity<List<User>> res = restTemplate.exchange("http://repository-service/user/find", HttpMethod.GET, null, 
//				new ParameterizedTypeReference<List<User>>(){});
//		
//		return res.getBody();
//	}
	
	@PostMapping("/add")
	public ResultModel add(@RequestParam String nric, @RequestParam String refno) {
		IamAccount iamAccount = restTemplate.getForObject("http://repository-service/user/get-by-refno?refno={refno}", 
				IamAccount.class, nric);
		ResultModel resultModel = new ResultModel();
		
		if(iamAccount != null){
			resultModel.setStatus(ResultModel.Status.FAIL.toString());
			resultModel.setMessage("user existed");
		} else {
			iamAccount = getDummyIamAccount(nric, refno);
			IamAccount res = restTemplate.postForObject("http://repository-service/user/account-opening", 
					iamAccount, IamAccount.class);
			resultModel.setStatus(ResultModel.Status.SUCCESS.toString());
			resultModel.setData(res);
		}
		return resultModel;
	}
	
	@GetMapping("/find-confirmed-account")
	public List<IamAccount> findConfirmedAccount(@RequestParam String nric) {
		ResponseEntity<List<IamAccount>> res = restTemplate.exchange("http://repository-service/user/find-confirmed-account-by-nric?nric={nric}", HttpMethod.GET, null, 
				new ParameterizedTypeReference<List<IamAccount>>(){}, nric);
		
		return res.getBody();
	}
	
	private IamAccount getDummyIamAccount(String nric, String refno){
		return new IamAccount(refno, "SG", "online", "BK", "TAN",
				null, null, "N", nric, "P",
				"Y", "test@test.com", "pending", "N", "R",
				"FSM", "P", "N", null, nric,
				new Date());
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	static class ResultModel {
		
		enum Status {
			SUCCESS("s"),
			FAIL("f");
			
			private String status;
			
			Status(String status){
				this.status = status;
			}
			
			@Override
			public String toString(){
				return status;
			}
		}
		
		private String status;
		private String message;
		private Object data;
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public Object getData() {
			return data;
		}
		public void setData(Object data) {
			this.data = data;
		}
	}
}
