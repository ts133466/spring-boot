package com.example.demo.restc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.IamAccount;
import com.example.demo.repository.IamAccountRepository;

@RestController
@RequestMapping("/user")
public class DemoRestController {

	@Autowired
	//	private UserRepository userRepository;
	private IamAccountRepository iamAccountRepository;

	//	@PostMapping("/add")
	//	public User add(@RequestParam String firstname, @RequestParam String lastname) {
	//		User user = new User(firstname, lastname);
	//		
	//		return userRepository.save(user);
	//	}
	//	
	//	@GetMapping("/get")
	//	public User get(@RequestParam String username) {
	//		return userRepository.findByUsername(username);
	//	}
	//	
	//	@GetMapping("/find")
	//	public List<User> find(){
	//		return (List<User>) userRepository.findAll();
	//	}

	@PostMapping("/account-opening")
	public IamAccount add(@RequestBody IamAccount iamAccount) {
		try{
			iamAccount = iamAccountRepository.save(iamAccount);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		return iamAccount;
	}

		@GetMapping("/get-by-refno")
		public IamAccount getByRefno(@RequestParam String refno) {
			return iamAccountRepository.getByRefno(refno);
		}
		
		@GetMapping("/find-by-nric")
		public List<IamAccount> getByNric(@RequestParam String nric) {
			return iamAccountRepository.findByNric(nric);
		}
		
		@GetMapping("/find-confirmed-account-by-nric")
		public List<IamAccount> find(@RequestParam String nric){
			return iamAccountRepository.findConfirmedAccountByNric(nric);
		}


}
