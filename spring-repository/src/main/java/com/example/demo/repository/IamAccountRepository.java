package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.demo.entity.IamAccount;

@RepositoryRestResource
public interface IamAccountRepository extends CrudRepository<IamAccount, String> {
	IamAccount getByRefno(@Param("q") String refno);
	List<IamAccount> findByNric(@Param("q") String nric);
	List<IamAccount> findById(@Param("q") String id);
	
	@Query("select i from IamAccount i where i.nric = :q and status = 'confirmed'")
	List<IamAccount> findConfirmedAccountByNric(@Param("q") String nric);
}
